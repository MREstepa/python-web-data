# Using Python to Access Web Data
This repository is for my [Using Python to Access Web Data](https://www.coursera.org/learn/python-network-data) module assignment answers.


## Meta
MREstepa - [Bitbucket](https://bitbucket.orh/MREstepa)

Repository Link: [Bitbucket](bitbucket.org/MREstepa/html-css-javascript/src)
